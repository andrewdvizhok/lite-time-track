var id = 0;
var g_setting=[];


//*===============================================
//=================================================
//функция для тестов */
function test(){
	//chrome.browserAction.setIcon({path:"48.png"});
	//window.open("https://ya.ru","_blank");
}
//======================================================
//*функция для копирования по двойному щелчку */
function copyClip(){
	//chrome.browserAction.setIcon({path:"48.png"});
	//window.open("https://ya.ru","_blank");
	//console.log(this.id);
	var id = this.id.split("-")[1];
	var task = this.id.split("-")[0];
	switch(task){
		case "c_task":
			var src = document.getElementById("protask-"+id);
			break;
		/*case "c_link":
			var src = document.getElementById("linkin-"+id);
			break;*/
		case "c_time":
			var src = document.getElementById("time-"+id);
			break;
		/*case "c_proj":
			var src = document.getElementById("proj-"+id);
			break;*/
		
	}
	if (src.disabled){
		src.disabled=false;
		src.select();
		src.disabled=true;
	}	
	src.select();
	document.execCommand("Copy");
}
//======================================================
/*функция для настроек */
function goOpt(){
	window.open("options.html?app","_self");
}

//======================================================
//*функция для перехода по ссылке */
function goLink(){
	//chrome.browserAction.setIcon({path:"48.png"});
	var id = this.id.split("-")[1];
	var lnk = document.getElementById("linkin-"+id).value;
	window.open(lnk,"_blank");
	//window.open("https://ya.ru","_blank");
	//console.log(lnk);
}
//======================================================
//*===============================================
//===============================================================================================================
//функция запуска/остановки таймера */
function control(){
	var len = this.src.split("/").length;
	//console.log(this.src.split("/"));
	//console.log(len);
	var btn = this.src.split("/")[len-1];
	var id = this.id.split("-")[1];
	var tmr = document.getElementById("time-"+id);
	
	switch (btn){
		case "play.png": 
			//проверка на мультитас, если разрешен запуск только одной задачи, то отключим все остальные
			if (document.getElementById("multitask").value == "radio"){myf_stop();}
			this.src="pause.png";
			this.value="1";
			//таймер будет моргать
			tmr.className="field_label_tmr blink";
			changeIcon("run");
			localStorage.setItem("ctrl_btn-"+id,"1");
			
			break;
		case "pause.png":
			this.src="play.png";
			this.value="0";
			localStorage.setItem("ctrl_btn-"+id,"0");
			localStorage.setItem("time-"+id,document.getElementById("time-"+id).placeholder);
			//таймер НЕ будет моргать
			tmr.className="field_label_tmr";
			changeIcon(checkStatus());
			//if (document.getElementById("multitask").value == "radio"){changeIcon("stop");}else{changeIcon(checkStatus());}
			break;
		case "accept.png":
			//var id = this.id.split("-")[1];
			var timer = document.getElementById("time-"+id);
			var res = timer.value.match(/[0-9]{2,3}:[0-9]{2}:[0-9]{2}/g);
			//document.getElementById("debug").innerHTML=res;
			if(res){ // выставляем время 
				timer.placeholder = SecToFtime(FtimeToSec(res[0]));
				//document.getElementById("debug").innerHTML=res;
				//timer.style.color=(document.getElementById("colorscheme").value == "dark")?"lightblue":"blue";
			}
			
			//break;
			//правим ссылку
			/*var links = document.getElementById("linkin-"+id);
			var lnkbtn = document.getElementById("link-"+id);*/
			//var nlink = document.getElementById("link-"+id);
			/*if (links.value != ""){
				var lnk = links.value.match(/^http[s]{0,1}:|^ftp[s]{0,1}:|^mailto:/g);
				lnkbtn.style.visibility = "visible";
				if (lnk){
					links.plcaeholder = links.value;
					//nlink.href = links;
					//nlink.innerHTML = links.substr(0,16);
				}else{
					var temp = links.value;
					links.value = "http://"+temp;
					links.plcaeholder = links.value;
					//nlink.href = "http://"+links;
					//nlink.innerHTML = "http://"+links.substr(0,16);
				}
			}else{
				links.placeholder = "";
				lnkbtn.style.visibility = "hidden";
			}*/
			//сохраним новый цвет задачи
			//var line = document.getElementById(id);
			
				
			saveTask(id); 
			//сохраним изменения в хранилище
			undoTask(id); 
			// внесли изменения, переведем иконки в прежний режим
			if(document.getElementById("chart").value=="block") chartBox();
			//обновим диаграммы
			break;
		
		//	undoTask(id);
		//	break;
	}
	
}
//================================================

//*===============================================
//===============================================================================================================
//меняем иконку в зависимости от состояния */
function changeIcon(status){
	switch(status){
		case "run":
			chrome.browserAction.setIcon({path:"49run.png"});
			//console.log("run");
			break;
		case "stop":
			chrome.browserAction.setIcon({path:"49stop.png"});
			break;
		case "nope":
			chrome.browserAction.setIcon({path:"49.png"});
			break;
	}
}

// проверяет статус, если есть хотя бы 1 задача во вкл, вернет run, если задач нет вернет nope
function checkStatus(){
	//console.log("test");
	if (document.getElementById("id").value!=""){
		var list = document.getElementById("id").value.split("|");
		
		for (var i=0; i<=list.length-2; i++){
			if(document.getElementById("ctrl_btn-"+list[i]).value=="1"){
				//console.log("test");
				return "run";
			}
		}
		return "stop";
	}else{
		return "nope";
	}
}

//===============================================

//*===============================================
//=================================================
//функция конвертации времени */
function FtimeToSec(ftime){
	return parseInt(ftime.split(":")[0])*3600+parseInt(ftime.split(":")[1])*60+parseInt(ftime.split(":")[2]);
}
function SecToFtime(sec){
	htime = Math.floor(sec/3600);
	mtime = Math.floor((sec - htime*3600)/60);
	stime = sec - htime*3600 - mtime*60;
	return ((htime<10)?"0"+htime:htime)+":"+((mtime<10)?"0"+mtime:mtime)+":"+((stime<10)?"0"+stime:stime);
}
function addTime(ftime,diff){
	return SecToFtime(parseInt(FtimeToSec(ftime)) + parseInt(diff));
}
//========================

//*===============================================
//============================================================================================================
//функция отсчета таймера */
function updateTime(){
	//var count = parseInt(document.getElementById("id").value); // сколько всего таймеров
	var timer;
	var time;
	var ftime,htime;
	var i=0;
	
	if (document.getElementById("id").value!=""){
		var list = document.getElementById("id").value.split("|");
		//console.log(document.getElementById("id").value);
		//list.splice(-1,1);
		//document.getElementById("id").value = list;//затолкаем данные в список, его использует getsetId()
		//подсчитаем сумму времени
		var sum_time = 0;
		for(var i=0;i<list.length-1; i++){
			timer = document.getElementById("time-"+list[i]);
			time = parseInt(FtimeToSec(timer.placeholder));
			if (document.getElementById("ctrl_btn-"+list[i]).value == "1"){
				time += 1;
				timer.placeholder = SecToFtime(time);
				//console.log(timer.style.border);
				if (timer.style.border == "0px" || timer.style.border == "") timer.value=timer.placeholder;
			}
			sum_time += time;
		}
		document.getElementById("sum-time").value = SecToFtime(sum_time);
	}
}
//======================================

//*===============================================
//==============================================================================================================
//функция добавления задачи */
function addTask(){
	//var body = document.getElementById("main");
	var src = document.getElementById("0");
	var cln = src.cloneNode(true);
	/* меняем название задачи */
	//=================================
	var task = document.getElementById("task");
	/*var links = document.getElementById("link");
	var proj = document.getElementById("proj");*/
	if (document.getElementById("multitask").value == "radio"){myf_stop();}
	var id = getsetId();//вставим задачу и получим ее номер
	//console.log(id);
	if (task.value == ""){
		restTask(id,"Some task..."+id,"00:00:00","1","#F2F2F2");
		//console.log(links.value);
	}else{
		restTask(id,task.value,"00:00:00","1","#F2F2F2");
	}
	//увеличиваем счетчик
	//id++;
	//сохраним задачу
	saveTask(id);
	changeIcon("run");
	//document.getElementById("id").value = id;
	task.value = "";
	/*links.value = "";
	proj.value = "";*/
	//подсчитаем кол-во задач
	document.getElementById("sum-task").value=document.getElementById("id").value.split("|").length-1;
	//нарисуем диаграммы если нужно
	if(document.getElementById("chart").value=="block") chartBox();
	//удалим сохраненные поля, т.к. уже создали новую задчу
	/*localStorage.removeItem("project");
	localStorage.removeItem("linkvalue");*/
	localStorage.removeItem("taskvalue");
}
//==================================
/*===============================================
============================================================================================================
функция контроля строки массива задач */
function getsetId(listing){ //найдем место в строке и получим номер задачи
	var id = 1;
	var list = "";
	//версия v1.1
	console.log("listing "+listing);
	if(listing){
		list = listing.split("|");
	}else{
		if(document.getElementById("id")){
			if (document.getElementById("id").value!=""){
				list = document.getElementById("id").value.split("|");
			}
			console.log("ok");
		}
	}
	
	
	
	if (list.length>0){
		id = (parseInt(list[list.length-2])+1);
		var rslt = id+"|";
		document.getElementById("id").value += rslt;
	}else{
		document.getElementById("id").value = "1|";
	}
	
	/*if (document.getElementById("id").value!=""){
		id = (parseInt(list[list.length-2])+1);//Номер элемента больше последнего		
		var rslt = id+"|";
		document.getElementById("id").value += rslt;//добавим элемент в конец строки
	}else{
		document.getElementById("id").value = "1|";//добавим элемент 
	}*/
	
	return id;
}


/*===============================================
============================================================================================================
функция восстановления задачи */
function restTask(id,protask,time,status,colort){
	var src = document.getElementById("0");
	var cln = src.cloneNode(true);
	cln.setAttribute("id",id); // задаем id задачи
	//вставляем новый элемент ----------------------------------
	document.getElementById("main").appendChild(cln);
	document.getElementById(id).style.display="block";//делаем его видимым
	
	//получаем только что созданный элемент
	var ncln = document.getElementsByName("protask-0")[1];
	/* меняем название задачи */
	//=================================
	ncln.value = protask;
	//меняем имя
	ncln.name="protask-"+id;
	//меняем ид
	ncln.id="protask-"+id;
	
	/*меняем название кнопок копирования, добавляем листенер кнопок для задачи, ссылки, таймера*/
	//====================================
	var ctask = document.getElementsByName("c_task-0")[1];
	ctask.id = "c_task-"+id;
	ctask.setAttribute("name","c_task-"+id);
	ctask.addEventListener("dblclick",copyClip);
	
	//меняем цвет
	cln.style.backgroundColor=colort;
	//console.log(cln.style.backgroundColor);
	//панель выбора цвета
	var ccolor = document.getElementsByName("color-0")[1];
	ccolor.id = "color-"+id;
	ccolor.setAttribute("name","color-"+id);
	var ccolorA = document.getElementsByName("colorA-0")[1];
	ccolorA.id = "colorA-"+id;
	ccolorA.setAttribute("name","colorA-"+id);
	ccolorA.addEventListener("click",setColor);
	var ccolorB = document.getElementsByName("colorB-0")[1];
	ccolorB.id = "colorB-"+id;
	ccolorB.setAttribute("name","colorB-"+id);
	ccolorB.addEventListener("click",setColor);
	var ccolorC = document.getElementsByName("colorC-0")[1];
	ccolorC.id = "colorC-"+id;
	ccolorC.setAttribute("name","colorB-"+id);
	ccolorC.addEventListener("click",setColor);
	var ccolorD = document.getElementsByName("colorD-0")[1];
	ccolorD.id = "colorD-"+id;
	ccolorD.setAttribute("name","colorD-"+id);
	ccolorD.addEventListener("click",setColor);
	var ccolorE = document.getElementsByName("colorE-0")[1];
	ccolorE.id = "colorE-"+id;
	ccolorE.setAttribute("name","colorE-"+id);
	ccolorE.addEventListener("click",setColor);
	var ccolorF = document.getElementsByName("colorF-0")[1];
	ccolorF.id = "colorF-"+id;
	ccolorF.setAttribute("name","colorF-"+id);
	ccolorF.addEventListener("click",setColor);
	
	//  вставис ссылку и поле ссылки если есть аднные
	/*if (links != ""){ // 
	console.log(links);*/
	//добавляем клик на линк для правки
	/*var clink = document.getElementsByName("c_link-0")[1];
	clink.id = "c_link-"+id;
	clink.setAttribute("name","c_link-"+id);*/
	/*меняем ид link */
	//================
	/*var nlink = document.getElementsByName("link-0")[1];
	nlink.id="link-"+id;
	nlink.name="link-"+id;*/
	/*меняем ид linkin */
	//================
	/*var nlinkin = document.getElementsByName("linkin-0")[1];
	nlinkin.id="linkin-"+id;
	nlinkin.name="linkin-"+id;
	if(document.getElementById("link").style.display=="none"){
		nlinkin.style.display="none";
		clink.style.display="none";
		nlink.style.display="none";
	}else{
		clink.addEventListener("dblclick",copyClip);
		nlink.addEventListener("click",goLink);
		//проверим ссылку, если не указали протокол, то это http
		if (links != ""){
			//ссылка есть делаем видимым кнопку
			nlink.style.visibility = "visible";
			var lnk = (links)?links.match(/^http[s]{0,1}:|^ftp[s]{0,1}:|^mailto:/g):"";
			if (lnk){
				nlinkin.value = links;
				nlinkin.placeholder = links;
				//nlink.href = links;
				//nlink.innerHTML = links.substr(0,16);
			}else{
				nlinkin.value = "http://"+links;
				nlinkin.placeholder = "http://"+links;
				//nlink.href = "http://"+links;
				//nlink.innerHTML = "http://"+links.substr(0,16);
			}
		}else{
			nlink.style.visibility = "hidden";
		}
	}*/
	//для копирования времени
	var ctime = document.getElementsByName("c_time-0")[1];
	ctime.id = "c_time-"+id;
	ctime.setAttribute("name","c_time-"+id);
	ctime.addEventListener("dblclick",copyClip);
	
	/*меняем название кнопки, добавляем листенер кнопки*/
	//====================================
	var nbtn = document.getElementsByName("ctrl_btn-0")[1];
	nbtn.id="ctrl_btn-"+id;
	nbtn.name="ctrl_btn-"+id;
	nbtn.addEventListener("click",control);
	nbtn.value=status; // таймер включен 
	nbtn.src=(status=="1"?"pause.png":"play.png");// ставим иконку включенного таймера
	/* переименоваываем и добавляем таймер*/
	//======================================
	var ntime = document.getElementsByName("time-0")[1];
	ntime.id="time-"+id;	
	ntime.name="time-"+id;
	ntime.value=time;
	ntime.placeholder=time;
	ntime.className = (status=="1"?"field_label_tmr blink":"field_label_tmr");//если таймер вкл, то моргаем
	/*меняем название кнопки удаления*/
	//=======================
	var nminus = document.getElementsByName("minus-0")[1];
	nminus.id="minus-"+id;
	nminus.name="minus-"+id;
	nminus.addEventListener("click",delTask);
	/*меняем ид edit */
	//================
	var nedit = document.getElementsByName("edit-0")[1];
	nedit.id="edit-"+id;
	nedit.name="edit-"+id;
	nedit.addEventListener("click",editTask);
	
	/*меняем ид reset */
	//================
	var nreset = document.getElementsByName("reset-0")[1];
	nreset.id="reset-"+id;
	nreset.name="reset-"+id;
	nreset.addEventListener("click",resetTime);
	/*меняем ид proj */
	//================
	/*var nproj = document.getElementsByName("proj-0")[1];
	nproj.id="proj-"+id;
	nproj.name="proj-"+id;
	nproj.value = project;*/
	//меняем ид переноса роекта
	/*var nprojbr = document.getElementsByName("projbr-0")[1];
	nprojbr.id = "nprojbr-"+id;
	nprojbr.setAttribute("name","nprojbr-"+id);*/
	/*меняем название кнопок копирования, добавляем листенер кнопок для проекта*/
	//====================================
	/*var cproj = document.getElementsByName("c_proj-0")[1];
	cproj.id = "c_proj-"+id;
	cproj.setAttribute("name","c_proj-"+id);
	//проверяем настройки, отображать ли нам proj
	if (document.getElementById("proj").style.display=="none" ){
		cproj.style.display="none";
		nproj.style.display="none";
		//nprojbr.style.display="none";
	}else{
		nproj.style.display="block";
		//nprojbr.style.display="block";
		cproj.addEventListener("dblclick",copyClip);
	}*/
	
	//нарисуем диаграммы если требуется
	var nchart = document.getElementsByName("chart-0")[1];
	nchart.id = "chart-"+id;
	nchart.setAttribute("name","chart-"+id);
	var nchartb = document.getElementsByName("chartb-0")[1];
	nchartb.id = "chartb-"+id;
	nchartb.setAttribute("name","chartb-"+id);
	nchart.style.display = document.getElementById("chart").value;
	nchartb.style.display = document.getElementById("chart").value;
	
}
//===============================================

/*===============================================
=================================================
функция удаления задачи или отмена действия редактирования */
function delTask(){
	var id = this.id.split("-")[1];
	//проверим действие, если отмена правки, то удалять не надо
	//if(document.getElementById("protask-"+id).disabled){
		var task = document.getElementById(id);
		//var timer = document.getElementById("time-"+id);
		//var ctrl = document.getElementById("ctrl_btn-"+id);
		var main = document.getElementById("main");
		main.removeChild(task);
			//console.log("nop");
		//}
		//проверим остались ли ещё элементы, если удалили последний, то очистим память
		var list = document.getElementById("id").value.replace(id+"|","");
		document.getElementById("id").value = list;
		if (list==""){//удалили все элементы очищаем
			myf_drop();
			
		}else{//не все элементы, удаляем текущий
			removeTask(id);
			changeIcon(checkStatus());
			// обновим диаграммы для остальных
			if(document.getElementById("chart").value=="block") chartBox(); 
		}	
	//подсчитаем кол-во задач
    document.getElementById("sum-task").value=document.getElementById("id").value.split("|").length-1;
}
//================================
/*===============================================
=================================================
функция изменения задачи*/
function myf_drop(){
	//var count = parseInt(document.getElementById("id").value);
	if(document.getElementById("id").value!=""){
		var list = document.getElementById("id").value.split("|");
		var main = document.getElementById("main");
		//var task;
		
		
		for(var i=0;i<list.length-1; i++){
			//removeTask(list[i]);
			if (document.getElementById(list[i])){
				var task = document.getElementById(list[i]);
				//console.log(task);
				main.removeChild(task);
			}
		}
	}
	document.getElementById("id").value=""; //обнулим счетчик
	//подсчитаем кол-во задач
    document.getElementById("sum-task").value=document.getElementById("id").value.split("|").length-1;
	var setting = localStorage.getItem("setting");
	localStorage.clear();
	localStorage.setItem("setting",setting);
	changeIcon("nope");
}
//================================
/*===============================================
=================================================
функция закрытия/открытия инструкции */
function clsNote(){
	if (document.getElementById("notes").style.display == "block"){
		document.getElementById("notes").style.display = "none";
	}else{
		document.getElementById("notes").style.display = "block";
	}
	//document.cookie = "test=test;t2=t2";
}
//===========================================================
/*===============================================
=================================================
функция задания цвета задачи */
function setColor(){
	var id = this.id.split("-")[1];
	var colort = this.getAttribute("color");
	var line = document.getElementById(id);
	//line.setAttribute('oldcolor',line.style.backgroundColor);
	line.style.backgroundColor = colort;
	//console.log(colort);
	//console.log(document.getElementById(id).style.backgroundColor);
}
/*===============================================
=================================================
функция работы с хранилищем */

function saveTask(id){ //сохраним задачу
	//var lnk = (document.getElementById("linkin-"+id).value)?document.getElementById("linkin-"+id).value:"";
	//var proj = document.getElementById("proj-"+id).value;
	localStorage.setItem("protask-"+id,document.getElementById("protask-"+id).value);
	localStorage.setItem("time-"+id,document.getElementById("time-"+id).placeholder);
	localStorage.setItem("ctrl_btn-"+id,document.getElementById("ctrl_btn-"+id).value);
	if(document.getElementById(id).style.backgroundColor==""){
		localStorage.setItem("color-"+id,"#F2F2F2");
	}else{
		localStorage.setItem("color-"+id,document.getElementById(id).style.backgroundColor);
	}
	//console.log(document.getElementById(id).style.backgroundColor);
	/*if (document.getElementById("linkin-"+id)) {
		localStorage.setItem("link-"+id,document.getElementById("linkin-"+id).value)
	}else{localStorage.removeItem("link-"+id)};*/
	/*if (document.getElementById("proj-"+id)) {
		localStorage.setItem("proj-"+id,document.getElementById("proj-"+id).value)
	}else{localStorage.removeItem("proj-"+id)};*/
	//console.log(lnk);
	localStorage.setItem("list",document.getElementById("id").value);

}
function removeTask(id){//удалим задачу
	localStorage.removeItem("protask-"+id);
	localStorage.removeItem("time-"+id);
	localStorage.removeItem("ctrl_btn-"+id);
	/*localStorage.removeItem("link-"+id);
	localStorage.removeItem("proj-"+id);*/
	localStorage.setItem("list",document.getElementById("id").value);
}

function getTask(storage,page,dt){//загрузим задачу
	var time = localStorage.getItem("time-"+storage);
	var task = localStorage.getItem("protask-"+storage);
	var status = localStorage.getItem("ctrl_btn-"+storage);
	var colort = localStorage.getItem("color-"+storage);
	/*var lnk = (localStorage.getItem("link-"+storage))?localStorage.getItem("link-"+storage):"";
	var proj = (localStorage.getItem("proj-"+storage))?localStorage.getItem("proj-"+storage):"";*/
	if(status=="1"){//таймер был включен добавляем разницу во времени
	//document.getElementById("task").value=addTime(localStorage.getItem("time-"+storage),dt);
	//console.log(dt);
		restTask(page,task,addTime(time,dt),status,colort);
		//document.getElementsByName("id").value = "getTask";
	}else{//иначе переносим как есть
		restTask(page,task,time,status,colort);
	}
	//console.log(colort);
	
}
/*===============================================
=================================================
функция импорта задач из файла*/
/*function myf_ImportCSV(){
	document.getElementById("fileinput").click();
}
function handleFileSelect(evt) {
    var input = evt.target;
    var reader = new FileReader();
    
    var id="1";
    reader.onloadstart = function(){
		localStorage.setItem("test","start");
	}
    
    reader.onload = function(){
    	
		var text = reader.result;
		var list = localStorage.getItem("list");
		var strings = text.split("\n");
		//console.log(strings);
		
		for (var i=1; i<strings.length; i++){
			var row = strings[i].split(";");
			//console.log(row);
			if (row.length == 5){
				if (list){
					id = getsetId(list);//вставим задачу и получим ее номер
				}else{
					list="";
				}
				console.log(id);
				list += id+"|";
				//console.log(id);
				//document.getElementById("anketa").value+=id;
				restTask(id,row[0],row[1],"0",row[3]);
				//saveTask(id);
				
				localStorage.setItem("protask-"+id,row[0]);
				localStorage.setItem("time-"+id,row[1]);
				localStorage.setItem("ctrl_btn-"+id,"0");
				if(document.getElementById(id).style.backgroundColor==""){
					localStorage.setItem("color-"+id,"#F2F2F2");
				}else{
					localStorage.setItem("color-"+id,row[3]);
				}
				
			}
		}
		localStorage.setItem("list",list);
		//console.log(strings);
	};
	
	reader.readAsText(input.files[0]);
 	//console.log(evt);
    //}
  }*/

/*===============================================
=================================================
функция экспорта задач в файл*/
function myfExportToCSV(){
	var list = localStorage.getItem("list").split("|");//извлечем список задач
	var time="",task="",status="",sec="", colort='';
	var textToSave = "Task;Time;Sec;Color;\n"
	console.log("export");
	for(var i=0;i<list.length-1; i++){//Пробежимся по всем элементам
		//time = localStorage.getItem("time-"+list[i]);
		time = document.getElementById("time-"+list[i]).placeholder;
		task = document.getElementById("protask-"+list[i]).value;
		colort = document.getElementById(list[i]).style.backgroundColor;
		/*if (document.getElementById("linkin-"+list[i])) {
			lnk = document.getElementById("linkin-"+list[i]).value;
		}
		if (document.getElementById("proj-"+list[i])) {
			proj = document.getElementById("proj-"+list[i]).value;
		}*/
		sec = FtimeToSec(time);
		textToSave += task+";"+time+";"+sec+";"+colort+";\n"
	}
	
    var textToSaveAsBlob = new Blob([textToSave], {type:"text/plain"});
    var textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);
    var fileNameToSaveAs = "myWork.csv";
 
    var downloadLink = document.getElementById("dlink");
    downloadLink.download = fileNameToSaveAs;
    //downloadLink.innerHTML = "Download File";
    downloadLink.href = textToSaveAsURL;
    //downloadLink.onclick = destroyClickedElement;
    //downloadLink.style.display = "none";
    //document.body.appendChild(downloadLink);
 
    downloadLink.click();
}

function destroyClickedElement(event)
{
    document.body.removeChild(event.target);
}

/////////////////////////////////////////////////


/*===============================================
=================================================
функция изменения задачи*/
function editTask(){
	// узнаем ид задачи
	var id = this.id.split("-")[1];
	var task = document.getElementById("protask-"+id);
	var colort = document.getElementById("color-"+id);
	var line = document.getElementById(id);
	/*var lnk = document.getElementById("linkin-"+id);
	var proj = document.getElementById("proj-"+id);*/
	//узнаем текущую цветовую схему
	var colorsch = "1px solid blue";//(document.getElementById("colorscheme").value == "dark")?"blue":"white";// == "mystyledark.css"
	//console.log(window.getComputedStyle(document.getElementById("proj")).backgroundColor);
	if (this.value == "edit"){
		//console.log(id);
		//Переключим для всех полей режим ввода, соотв. ид
		
		task.disabled = false;
		
		task.style.border = colorsch;
		task.placeholder = task.value;
		//var lnk = document.getElementById("link-"+id);
		
		//меняем для ссылки
		/*lnk.disabled = false;
		lnk.style.border = colorsch;
		lnk.placeholder = lnk.value;//забэкапим значение ссылки
		//меняем для проекта
		proj.disabled = false;
		proj.style.border = colorsch;
		proj.placeholder = proj.value;//бэкапим для проетка*/
		
		//отобразить кнопки выбора цвета задачи
		colort.style.display="block";
		//сохраним старый цвет задачи
	    line.setAttribute('oldcolor',line.style.backgroundColor);
		
		var timer = document.getElementById("time-"+id);
		timer.disabled = false;
		timer.style.border = colorsch;
		//меняем кнопки
		this.src = "undo.png";
		this.value = "undo";
		document.getElementById("ctrl_btn-"+id).src = "accept.png";
	}else{ // отменили изменения
		this.value = "edit";
		this.src = "edit.png"
		/*lnk.value = lnk.placeholder;*/ //восстановим значение ссылки
		task.value = task.placeholder;
		/*proj.value = proj.placeholder;*/
		
		//возвращаем старый цвет
		line.style.backgroundColor = line.getAttribute('oldcolor');
		
		undoTask(id);
	}
	
	
}
function undoTask(id){
	//Переключим для всех полей режим ввода, соотв. ид
	var task = document.getElementById("protask-"+id);
	var colort = document.getElementById("color-"+id);
	var line = document.getElementById(id);
	/*var lnk = document.getElementById("linkin-"+id);
	var proj = document.getElementById("proj-"+id);*/
	task.disabled = true;
	task.style.border = "1px solid grey";
	/*proj.disabled = true;
	proj.style.border = "0";*/
	colort.style.display="none";
	//document.getElementById("linkin-"+id).style.display = "none";
	//document.getElementById("link-"+id).style.display = "block";
	/*lnk.disabled = true;
	lnk.style.border = "0";*/
	var timer = document.getElementById("time-"+id);
	timer.disabled = true;
	timer.style.border = "0";
	//меняем кнопки
	document.getElementById("edit-"+id).src = "edit.png";
	document.getElementById("edit-"+id).value = "edit";
	document.getElementById("ctrl_btn-"+id).src = (document.getElementById("ctrl_btn-"+id).value == "0")?"play.png":"pause.png";
	
	
}

function resetTime(){
	var id = this.id.split("-")[1];
	//console.log(id);
	var timer = document.getElementById("time-"+id);
	timer.placeholder="00:00:00";
	timer.value="00:00:00";
	localStorage.setItem("time-"+id,"00:00:00");
	if(document.getElementById("chart").value=="block") chartBox();//обновим диаграммы
}
/*===============================================
=================================================
Рисуем диаграммы*/
function chartBox(){
	if (localStorage.getItem("list")){
		var list = localStorage.getItem("list").split("|");//пробежимся по всем таймерам
		var maxTime = 0;
		
		var curTime = 0;
		var rc=0; 
		var gc=0;
		var widthBox = 10;
		
		var minTime = FtimeToSec(document.getElementById("time-"+list[0]).placeholder);
		for(var i=0;i<list.length-1; i++){//ищем min max массива
			curTime = FtimeToSec(document.getElementById("time-"+list[i]).placeholder);	
			if (curTime>maxTime){maxTime = curTime;}
			if (curTime<minTime){minTime = curTime;}
		}
		var deltaTime = maxTime - minTime;
		for(var i=0;i<list.length-1; i++){
			curTime = FtimeToSec(document.getElementById("time-"+list[i]).placeholder);	
			if(maxTime>90){
				//gc = Math.round(255-((curTime-minTime)/deltaTime)*255);
				rc = ((curTime-minTime)/deltaTime);
				widthBox = Math.round(5+((curTime-minTime)/deltaTime)*90);
				// проверяем световую схему
				switch (document.getElementById("colorscheme").value){
					case "dark":
						document.getElementById("chartb-"+list[i]).style.backgroundColor="rgb("+Math.round(71+rc*164)+","+Math.round(180-rc*129)+","+Math.round(255-rc*182)+")";
					break;
					case "pink":
						document.getElementById("chartb-"+list[i]).style.backgroundColor="rgb("+Math.round(52-rc*41)+","+Math.round(7+rc*215)+","+Math.round(69+rc*60)+")";
					break;
					default:
						document.getElementById("chartb-"+list[i]).style.backgroundColor="rgb(249,"+Math.round(212-rc*158)+","+Math.round(35-rc*35)+")";
				}
				document.getElementById("chartb-"+list[i]).style.width=widthBox+"%";
				//document.getElementById("chartb-"+list[i]).style.background="green;";
				//document.getElementById("chartb-"+list[i]).style.backgroundColor="white";
				//console.log("rgb="+rc+","+gc);
			}else{
				document.getElementById("chartb-"+list[i]).style.backgroundColor="green";
				document.getElementById("chartb-"+list[i]).style.width="5%";
				//console.log("green");
			}
		}
	}
}
//=================================================
//функция для направления на донаты
function myf_goYM(){
	//window.open("https://money.yandex.ru/quickpay/shop-widget?writer=seller&targets=%D0%9D%D0%B0%20%D0%B5%D0%B4%D1%83%20%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%87%D0%B8%D0%BA%D0%B0%D0%BC%20%2F%20For%20food%20of%20dev&targets-hint=&default-sum=10&button-text=14&payment-type-choice=on&comment=on&hint=&successURL=&quickpay=shop&account=410015066499354","_blank");
	window.open("https://money.yandex.ru/to/410015066499354","_blank");
}
function myf_goSTT(){
	window.open("https://chrome.google.com/webstore/detail/simple-time-track/nbdpdmcachnlljedfoflcialaigppapk","_blank");
	//window.open("https://addons.mozilla.org/en-US/firefox/addon/simple-time-track-v2","_blank");
}
//направляем на анкету
function myf_goAnketa(){
	window.open("https://docs.google.com/forms/d/e/1FAIpQLScMU-8J8t1Z_xDHcd5UVnl_CKCr13AmcoImqmKSSDqcO7_R8g/viewform","_blank");
	var set = localStorage.getItem("setting");
	set += "ankt;";
	localStorage.setItem("setting",set);
	
}
//направляем на оценку приложения
function myf_goSTAR(){
	//window.open("https://chrome.google.com/webstore/detail/simple-time-track/nbdpdmcachnlljedfoflcialaigppapk","_blank");
	window.open("https://addons.mozilla.org/en-US/firefox/addon/simple-time-track-v2","_blank");
}
//=================================================
//функция для сброса всех таймеров
function myf_resetAll(){
	//проверим есть ли задачи
	if(document.getElementById("id").value!=""){
		//сформируем список задач
		var list = document.getElementById("id").value.split("|");
		//пробежимся по все задачам
		for(var i=0;i<list.length-1; i++){
			//сбросим все таймеры
			document.getElementById("reset-"+list[i]).click();
		}
	}
}
//=================================================
//функция остановки всех таймеров
function myf_stop(){
	// остановим все таймеры
	var list = document.getElementById("id").value.split("|");
	//пробежимся по всем задачам
	for(var i=0;i<list.length-1; i++){
		//остановим все таймеры
		if (document.getElementById("ctrl_btn-"+list[i]).value=="1"){
			var btnplay = document.getElementById("ctrl_btn-"+list[i]).src.split("/")[document.getElementById("ctrl_btn-"+list[i]).src.split("/").length-1];
			if (btnplay != "pause.png"){undoTask(list[i]);}
			//document.getElementById("ctrl_btn-"+list[i]).click();
			document.getElementById("ctrl_btn-"+list[i]).src="play.png";
			document.getElementById("ctrl_btn-"+list[i]).value="0";
			document.getElementById("time-"+list[i]).className="field_label_tmr";
			localStorage.setItem("ctrl_btn-"+list[i],"0");
			localStorage.setItem("time-"+list[i],document.getElementById("time-"+list[i]).placeholder);
			
		}
	}
}
//=================================================
//функция для переключения характера отсчета таймеров, (только один или несколько)
function myf_multitask(){
	//поменяем иконку кнопки и сохраним выбранное значение
	var len = this.src.split("/").length;
	var btn = this.src.split("/")[len-1];
	var btnplay;
	
	if (btn=="chbox.png"){
		this.src="radio.png";
		this.value="radio";
		localStorage.setItem("multitask","radio.png");
		myf_stop();
		changeIcon("stop");
	}else{
		this.src="chbox.png";
		this.value="";
		localStorage.setItem("multitask","chbox.png");
	}
}
//=================================================
//функция для получения задач из джиры
/*function myf_getJIRA(){
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'http://localhost:8080/rest/api/2/search?jql=project=demo', true);
	// If specified, responseType must be empty string or "text"
	xhr.responseType = 'text';
	xhr.onload = function () {
    if (xhr.readyState === xhr.DONE) {
        if (xhr.status === 200) {
            //console.log(xhr.response);
            //console.log(xhr.responseText);
            document.getElementById("jira").value = xhr.responseText;
            console.log(JSON.parse(xhr.responseText));
            //через время вызовем функцию для создания задач в таймере
            setTimeout(myf_createJIRA, 500);
        }
    }
	};
	xhr.send(null);
}*/
//=========================================================
//запишем задачи в нащ таймер
/*function myf_createJIRA(){
	
	var jsonJIRA = JSON.parse(document.getElementById("jira").value);
	var count = jsonJIRA.issues.length;
	console.log(count);
	for(var i=0;i<count;i++){
		console.log(jsonJIRA.issues[i].key);
		console.log(jsonJIRA.issues[i].fields.summary);
		
	}
	
}*/
//=================================================
//функция открытия в новом окне
/*function myf_expand(){
	//высота 100+ 79*кол-во задач
	if(document.getElementById("id").value!=""){
		//сформируем список задач
		
		//вычеслим высоту
		var count = 90+parseInt(document.getElementById("id").value.split("|").length)*79;
		console.log(count);

	}
	//window.open("popup.html","_blank","height="+count+",width=430");
	window.close();
}*/


//================================================

//LISTENERS and STORAGE------------------------------------------------------
window.onload=function(){
	//прочетм куки для применения настроек
	//var coockie = document.cookie;
	if (localStorage.getItem("setting")){
		var setting = localStorage.getItem("setting").split(";");
		for(var i=0;i<setting.length;i++){
			switch(setting[i]){
				/*case "proj":
					document.getElementById("proj").style.display="block";
					break;*/
				/*case "nolink":
					document.getElementById("link").style.display="none";
					break;*/
				case "nodrop":
					document.getElementById("drop").style.display="none";
					break;
				case "nohelp":
					document.getElementById("qst").style.display="none";
					break;	
				case "chart":
					document.getElementById("chart").value="block";
					break;	
				case "reset":
					document.getElementById("reset").style.display="inline-block";
					break;		
				case "notCSV":
					document.getElementById("exportCSV").style.display="none";
					document.getElementById("importCSV").style.display="none";
					break;	
				case "notmultitask":
					document.getElementById("multitask").style.display="none";
					break;
				/*case "dark":
					document.getElementById("stylesheet").href = "mystyledark.css";
					document.getElementById("colorscheme").value = "dark";
					break;
				case "pink":
					document.getElementById("stylesheet").href = "mystylepink.css";
					document.getElementById("colorscheme").value = "pink";
					break;*/
				case "ankt":
					document.getElementById("anketa").style.display="none";
					break;
					
			}
		}
	}
	//прочет ранее введные поля новой задачи, чтобы не потерялись после сворачивания
	/*if (localStorage.getItem("project")){ document.getElementById("proj").value = localStorage.getItem("project");}*/
	if (localStorage.getItem("taskvalue")){ document.getElementById("task").value = localStorage.getItem("taskvalue");}
	/*if (localStorage.getItem("linkvalue")){ document.getElementById("link").value = localStorage.getItem("linkvalue");}*/
	
	//прочтем параметр мультитаска, чтобы знать характер работы таймера
	if (localStorage.getItem("multitask") == "radio.png"){ 
		document.getElementById("multitask").src = "radio.png";
		document.getElementById("multitask").value = "radio";
	}else{
		document.getElementById("multitask").src = "chbox.png";
		document.getElementById("multitask").value = "";
	}
	
	document.getElementById("add").addEventListener("click",addTask);
	document.getElementById("drop").addEventListener("click",myf_drop);
	document.getElementById("cls_note").addEventListener("click",clsNote);
	document.getElementById("qst").addEventListener("click",clsNote);
	document.getElementById("setting").addEventListener("click",goOpt);
	//document.getElementById("star").addEventListener("click",myf_goSTAR);
	document.getElementById("ym").addEventListener("click",myf_goYM);
	document.getElementById("goYM").addEventListener("click",myf_goYM);
	document.getElementById("reset").addEventListener("click",myf_resetAll);
	document.getElementById("exportCSV").addEventListener("click",myfExportToCSV);
	document.getElementById("multitask").addEventListener("click",myf_multitask);
	//document.getElementById("importCSV").addEventListener("click",myf_ImportCSV);
	//document.getElementById("importCSV").addEventListener("click", handleFileSelect, true);
	//document.getElementById("fileinput").addEventListener("change", handleFileSelect, false);
	//document.getElementById("fileinput").addEventListener("click", handleFileSelect, false);
	document.getElementById("anketa").addEventListener("click",myf_goAnketa);
	document.getElementById("goSTT").addEventListener("click",myf_goSTT);
	//document.getElementById("popup").addEventListener("click",myf_getJIRA);
	document.getElementById("task").addEventListener("keyup",function(event) {
    	event.preventDefault();
    		if (event.keyCode == 13) {
        		document.getElementById("add").click();
        	}
    	});
    /*document.getElementById("link").addEventListener("keyup",function(event) {
    	event.preventDefault();
    		if (event.keyCode == 13) {
        		document.getElementById("add").click();
        	}
    	});
    document.getElementById("proj").addEventListener("keyup",function(event) {
    	event.preventDefault();
    		if (event.keyCode == 13) {
        		document.getElementById("add").click();
        	}
    	});*/
	// прочитаем данные из хранилища для восстановления таймера
	if (typeof(Storage) !== "undefined") {
		var timestamp = new Date();//посмотрим разницу
		if (localStorage.getItem("timestamp")){
			var difftime = Math.round(timestamp.getTime()/1000)-parseInt(localStorage.getItem("timestamp"));
		}else{
			var difftime = 0;
		}
	// новый тип хранилища v1.1*/
    	var realcnt = 1;
		//document.getElementById("task").value="v1.1";
		if(localStorage.getItem("list")){ //данные есть
			//document.getElementById("id").value = localStorage.getItem("list");
			//console.log(document.getElementById("id").value);
			document.getElementById("id").value = localStorage.getItem("list");
			var list = localStorage.getItem("list").split("|");
			//list.splice(-1,1);
			//document.getElementById("id").value = list;//затолкаем данные в список, его использует getsetId()
			for(var i=0;i<list.length-1; i++){
				getTask(list[i],list[i],difftime);
				
			}
		}
    }
    changeIcon(checkStatus());
    if(document.getElementById("chart").value=="block") chartBox();
    //подсчитаем кол-во задач
    document.getElementById("sum-task").value=document.getElementById("id").value.split("|").length-1;
}

window.onblur=function(){
	/// сохраним таймер в хранилище
	if (document.getElementById("id").value!=""){
		var date = new Date();
		localStorage.setItem("timestamp",Math.round(date.getTime()/1000));
		localStorage.setItem("list",document.getElementById("id").value);
		var list = document.getElementById("id").value.split("|");
		for(var i=0;i<list.length-1; i++){
			if (document.getElementById("ctrl_btn-"+list[i]).value=="1"){
				localStorage.setItem("time-"+list[i],document.getElementById("time-"+list[i]).placeholder);
			}
		}
	}
	//сохраним введеные значения в поля, чтобы можно было править после соврачивания
	/*localStorage.setItem("project",document.getElementById("proj").value);*/
	localStorage.setItem("taskvalue",document.getElementById("task").value);
	/*localStorage.setItem("linkvalue",document.getElementById("link").value);*/
    changeIcon(checkStatus());//обновим иконк
}

//----------------------------------------------END LISTENERS-----

//TIMERS------------------------------------------------------
window.setInterval(updateTime,1000);
//var btn = document.getElementById("ctrl_btn");

//----------------------------------------------END TIMERS-----